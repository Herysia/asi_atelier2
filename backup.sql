PGDMP     ,                    x           ASI    12.2    12.2                 0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            !           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            "           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            #           1262    17994    ASI    DATABASE     �   CREATE DATABASE "ASI" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE "ASI";
                postgres    false            $           0    0    DATABASE "ASI"    ACL     #   GRANT ALL ON DATABASE "ASI" TO tp;
                   postgres    false    2851            �            1259    18315    auth    TABLE     �   CREATE TABLE public.auth (
    id character varying(255) NOT NULL,
    expiration timestamp without time zone,
    user_id integer
);
    DROP TABLE public.auth;
       public         heap    tp    false            �            1259    18320    cards    TABLE     T  CREATE TABLE public.cards (
    id integer NOT NULL,
    attack integer NOT NULL,
    defence integer NOT NULL,
    description character varying(255),
    energy integer NOT NULL,
    family character varying(255),
    hp integer NOT NULL,
    img_url character varying(255),
    name character varying(255),
    price integer NOT NULL
);
    DROP TABLE public.cards;
       public         heap    tp    false            �            1259    18313    hibernate_sequence    SEQUENCE     {   CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.hibernate_sequence;
       public          tp    false            �            1259    18328    items    TABLE     b   CREATE TABLE public.items (
    id integer NOT NULL,
    item_id integer,
    owner_id integer
);
    DROP TABLE public.items;
       public         heap    tp    false            �            1259    18333    users    TABLE     �   CREATE TABLE public.users (
    id integer NOT NULL,
    money integer NOT NULL,
    name character varying(255),
    password character varying(255),
    surname character varying(255)
);
    DROP TABLE public.users;
       public         heap    tp    false                      0    18315    auth 
   TABLE DATA           7   COPY public.auth (id, expiration, user_id) FROM stdin;
    public          tp    false    203   \                 0    18320    cards 
   TABLE DATA           k   COPY public.cards (id, attack, defence, description, energy, family, hp, img_url, name, price) FROM stdin;
    public          tp    false    204   �                 0    18328    items 
   TABLE DATA           6   COPY public.items (id, item_id, owner_id) FROM stdin;
    public          tp    false    205   �                 0    18333    users 
   TABLE DATA           C   COPY public.users (id, money, name, password, surname) FROM stdin;
    public          tp    false    206          %           0    0    hibernate_sequence    SEQUENCE SET     @   SELECT pg_catalog.setval('public.hibernate_sequence', 4, true);
          public          tp    false    202            �
           2606    18319    auth auth_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.auth
    ADD CONSTRAINT auth_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.auth DROP CONSTRAINT auth_pkey;
       public            tp    false    203            �
           2606    18327    cards cards_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.cards
    ADD CONSTRAINT cards_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.cards DROP CONSTRAINT cards_pkey;
       public            tp    false    204            �
           2606    18332    items items_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.items DROP CONSTRAINT items_pkey;
       public            tp    false    205            �
           2606    18342 "   users uk_c7axdovioy5258of4drgpw13v 
   CONSTRAINT     `   ALTER TABLE ONLY public.users
    ADD CONSTRAINT uk_c7axdovioy5258of4drgpw13v UNIQUE (surname);
 L   ALTER TABLE ONLY public.users DROP CONSTRAINT uk_c7axdovioy5258of4drgpw13v;
       public            tp    false    206            �
           2606    18340    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            tp    false    206            �
           2606    18348 !   items fk6vee251nl25xn26yutfvv28vw    FK CONSTRAINT     �   ALTER TABLE ONLY public.items
    ADD CONSTRAINT fk6vee251nl25xn26yutfvv28vw FOREIGN KEY (item_id) REFERENCES public.cards(id);
 K   ALTER TABLE ONLY public.items DROP CONSTRAINT fk6vee251nl25xn26yutfvv28vw;
       public          tp    false    204    2705    205            �
           2606    18353 !   items fke37yi0i6rmaqcqickvb1vty22    FK CONSTRAINT     �   ALTER TABLE ONLY public.items
    ADD CONSTRAINT fke37yi0i6rmaqcqickvb1vty22 FOREIGN KEY (owner_id) REFERENCES public.users(id);
 K   ALTER TABLE ONLY public.items DROP CONSTRAINT fke37yi0i6rmaqcqickvb1vty22;
       public          tp    false    205    2711    206            �
           2606    18343     auth fkpv45mvdt7km1gvrtn5f9rsvd7    FK CONSTRAINT        ALTER TABLE ONLY public.auth
    ADD CONSTRAINT fkpv45mvdt7km1gvrtn5f9rsvd7 FOREIGN KEY (user_id) REFERENCES public.users(id);
 J   ALTER TABLE ONLY public.auth DROP CONSTRAINT fkpv45mvdt7km1gvrtn5f9rsvd7;
       public          tp    false    203    206    2711            �           826    18132    DEFAULT PRIVILEGES FOR TABLES    DEFAULT ACL     G   ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TABLES  TO tp;
                   postgres    false               g   x�=�9�0�:F����K���j�H���*�e���M�5�ͨv�]c_����yp��]� 1�Y��;�EP_>)�_�2�t�s������'��~�a�         �  x�eT�r�6<�_��.�D���Y�8eW6��R��H��MZ<T�~M*�����@�nU.$1=�=Sf��VE�1Xm��?���`���Ť(D-����P� Q�9('Z)aתJ�
'�P�1X���aeq
��Ii9�ʫ"��M�J�!���ؑ�M���F=d�A���$+'EV΋�N
k�rbg�'��#-�Qy�9 �	V�JP�ֲC��V������n�rv�Lm-�"/Gl�/gͧYY�"�E���X2~ځ����⃪LK)�Ǿ�\��&�ZX��_���+p���j%�"�}��O2f|O�Sg�3�/Ғ76�;�>��P��98���8�8�!*��J]�Ž�����������J%� ���؎t�RM�Ě�}��OE(7q�,�@Q�����VF�:�t���I���u�x�Om �.h�\��n@6Ҭ�%~�N�(*k*S��I��%�ʲȗ�d��H���A>ɪf�S!|O�Jh�g���ҿ�7��N՞*�!s�k0���P���W�Ǎ"��v�r>I��kv�%�6��	ʯ�1���h�!���+��M.✞ZɄMEm̉ ���=��ƹ5�u������(���}~�1N�#�����e����g����ŎK�L,��6V5�gH,�Uŭ��^^��aecl��)��^��Z��=��Ym:@5�uZ����4��(0O�<�*�Pyy(z��1��TGi��k��KQXU�V�=��d����\Z����� �%5�� �E�"~�d�K!�u��o��� nІ
���8gq���$&����)�.ZC1c.�ޣ�%�40��p��[�q5�����馁V�kp#�k�vD��&h_��2=6\6�����'.Q�����mY	+�M�����x�]������'��i�ǡ��W�E�,V��rU�W�b>O��rv]l����l>+���z�k'E�n;�e���v].��_�eǵ���Q��� ��?�         9   x��[  ���Na0^&P0�:�&MN'�5��t��L�X�p�W~Cl��bf�"�         �   x�M��B1 �d��s�l� �4��
�ģa{�(��+�8*�3��O��2�lI�c�6�Hv�Ŝ��E��C[�mz�%��]軩[�PN����2��	��[��d�4��	(ƾ��N�D1Qf�`�嘏�~�z>�Z��6     