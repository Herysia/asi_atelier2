/**
 * Stock les données utilisateur pour pouvoir s'en servir facilement
 */
let userData = {};

/**
 * Vérifie si un utilisateur est déjà connecté (a un auth-token valide), si ce n'est pas le cas, rediriges à Login, si c'est le cas et qu'il est sur la page de login, regidire à Home
 * Si l'utilisateur est valide: Met à jour ses données personnelles
 */
function getUserData() {
    $.get('/api/getUserData', function (data) {
        if(!cookieRedirect() && data) {//We should update userData
            userData = data;
            updateUserData(data);

        }
    });
}

/**
 * Vérifie si un utilisateur est déjà connecté (a un auth-token valide), si ce n'est pas le cas, rediriges à Login, si c'est le cas et qu'il est sur la page de login, regidire à Home
 *
 */
function cookieRedirect() {
    if(!Cookies.get('auth-token') && window.location.pathname !== "/login.html") {
        window.location.replace('/login.html')
    }
    else if (Cookies.get('auth-token') && window.location.pathname === "/login.html") {
        window.location.replace('/home.html')
    }
    else
        return false;

    return true;//We should never come here, unless client has disabled location.replace
}

function updateUserData() {
    //TODO: update fields with userdata
    $('#userNameId')[0].innerHTML = `${userData.name} ( ${userData.surname} )`;
    $('#userMoney')[0].innerHTML = (userData.money/100).toFixed(2);
}

$(document ).ready(function() {
    getUserData();
});