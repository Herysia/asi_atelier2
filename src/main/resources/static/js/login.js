/**
 * Login form validation
 */
$('#loginForm')
    .form({
        fields: {
            username: 'empty',
            password: 'empty'
        }
    })
;

/**
 * Register form validation
 */
$('#registerForm')
    .form({
        fields: {
            name: 'empty',
            surname: 'empty',
            password : ['minLength[4]', 'empty'],
            repeatPassword: ['match[registerPassword]']
        }
    })
;

/**
 * Callback for login button
 * Verifie les données
 * Envoie les données
 */
$('#loginForm').submit(event => {
    event.preventDefault();
    let $form = $('#loginForm');
    const keys = ['username', 'password'];

    //check form values
    for(const key of keys) {
        if(!$form.form('is valid', key))
            return;
    }
    $.ajax({
        url: '/api/login',
        type: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify( $form.form('get values', keys) ),
        error: function(err) {
            console.warn(err);
        },
        success: function(res) {
            window.location.replace('/home.html')
        }
    });
});

/**
 * Callback for register button
 * Verifie les données
 * Envoie les données
 */
$('#registerForm').submit(event => {
    event.preventDefault();
    let $form = $('#registerForm');
    const sentKeys = ['name', 'surname', 'password'];
    const validKeys = ['repeatPassword']

    //check form values
    for(const key of sentKeys.concat(validKeys)) {
        if(!$form.form('is valid', key))
            return;
    }
    $.ajax({
        url: '/api/register',
        type: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify( $form.form('get values', sentKeys) ),
        error: function(err) {
            console.warn(err);
        },
        success: function(res) {
            window.location.replace('/home.html')
        }
    });
});

$('#toggleRegister').click(switchLoginRegisterForm);
$('#toggleLogin').click(switchLoginRegisterForm);
function switchLoginRegisterForm() {
    $('#loginForm').toggle();
    $('#registerForm').toggle();
}

//https://stackoverflow.com/questions/11338774/serialize-form-data-to-json
function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}