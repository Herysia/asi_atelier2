let actionString = "Sell";
function fillCurrentCard(family,imgUrl,name,description,hp,energy,defence,attack,price,cardId){
    //FILL THE CURRENT CARD
    $('#selectedCard')[0].setAttribute('name', cardId);
    $('#cardFamilyNameId')[0].innerText=family;
    $('#cardImgId')[0].src=imgUrl;
    $('#cardNameId')[0].innerText=name;
    $('#cardDescriptionId')[0].innerText=description;
    $('#cardHPId')[0].innerText=hp+" HP";
    $('#cardEnergyId')[0].innerText=energy+" Energy";
    $('#cardAttackId')[0].innerText=attack+" Attack";
    $('#cardDefenceId')[0].innerText=defence+" Defence";
    $('#cardPriceId')[0].innerText=price;
};


function parseCardContentToList(family,imgUrl,name,description,hp,attack,energy,defence,price){
    
    let content="\
    <td> \
    <img  class='ui avatar image' src='"+imgUrl+"'> <span>"+name+"</span> \
   </td> \
    <td>"+description+"</td> \
    <td>"+family+"</td> \
    <td>"+hp+"</td> \
    <td>"+energy+"</td> \
    <td>"+attack+"</td> \
    <td>"+defence+"</td> \
    <td><span>"+(parseInt(price)/100).toFixed(2)+"</span> €</td>\
    <td>\
        <div class='ui vertical animated button' tabindex='0''>\
            <div class='hidden content'>"+actionString+"</div>\
    <div class='visible content'>\
        <i class='shop icon'></i>\
    </div>\
    </div>\
    </td>";
    return content;
};

function addCardToList(item) {
    let card = item.itemId;
    $('#cardListId tbody').append('<tr id='+item.id+'>'+parseCardContentToList(card.family,card.imgUrl,card.name,card.description,card.hp,card.energy,card.defence,card.attack,card.price)+'</tr>');
    $('#'+item.id).click(onClickListCard);
    $('#'+item.id).find('.button').click(buySellCallbackList)
}
function onClickListCard(event) {
    let element = event.currentTarget;
    let values = $(element).find('td');
    fillCurrentCard(
        values[2].innerHTML,//family
        $(values[0]).find('img')[0].src,//imgUrl
        $(values[0]).find('span')[0].innerHTML,//name
        values[1].innerHTML,//description
        values[3].innerHTML,//hp
        values[4].innerHTML,//energy
        values[5].innerHTML,//defence
        values[6].innerHTML,//attack
        $(values[7]).find('span')[0].innerHTML,//price
        element.id//id
    )
}
function buySellCallbackList(event) {
    event.stopPropagation();
    let id = $(event.currentTarget).parents('tr')[0].id;
    buySellCallback(id)
}
function buySellCallbackCard(event) {

    let id = $('#selectedCard')[0].getAttribute('name');
    buySellCallback(id)
}
function removeFromListById(id) {
    $('#'+id).remove();
    if($('#selectedCard')[0].getAttribute('name') == id) {
        let element = $('#cardListId tbody tr')[0];
        if(!element) {
            fillCurrentCard("", "", "No card", "", 0,0,0,0,0, -1);
            return;
        }
        let values = $(element).find('td');
        fillCurrentCard(
            values[2].innerHTML,//family
            $(values[0]).find('img')[0].src,//imgUrl
            $(values[0]).find('span')[0].innerHTML,//name
            values[1].innerHTML,//description
            values[3].innerHTML,//hp
            values[4].innerHTML,//energy
            values[5].innerHTML,//defence
            values[6].innerHTML,//attack
            $(values[7]).find('span')[0].innerHTML,//price
            element.id//id
        )
    }
}
function buySellCallback(id) {
    if(actionString == "Buy") {
        $.get('/api/buyCard', {id: parseInt(id)}, function(data) {
            if(data && JSON.parse(data).success) {
                getUserData();
                removeFromListById(id);
                //alert("Card bought.")
            }
            else {
                alert(data);
            }
        }).fail((res, code, msg) => {
            if(code == 401 || code == 403)
                cookieRedirect();
        });
    }
    else if(actionString == "Sell") {
        $.get('/api/sellCard',  {id: id}, function(data) {
            if(data && JSON.parse(data).success) {
                getUserData();
                removeFromListById(id);

                //alert("Card Sold.")
            }
            else {
                alert(data);
            }
        }).fail((res, code, msg) => {
            if(code == 401 || code == 403)
                cookieRedirect();
        });
    }
}
function buyRandomCardCallback() {
    $.get('/api/buyRandomCard', function(data) {
        if(data && JSON.parse(data).success) {
            getUserData();
            //alert("Card bought.")
        }
        else {
            alert(data);
        }
    }).fail((res, code, msg) => {
        if(code == 401 || code == 403)
            cookieRedirect();
    });
}