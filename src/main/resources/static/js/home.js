$("#playButtonId").click(function(){
    alert("Comming soon !");
});

$("#buyButtonId").click(function(){
    window.location.hash = "buyPage";
    updateView();
});

$("#sellButtonId").click(function(){
    window.location.hash = "sellPage";
    updateView();
});

$("#logoutButton").click(function(){
    $.get("/api/logout", function(){
        window.location.replace("/login.html");
    })
})

$('#returnHomeButton').click(function(){
    window.location.hash = "mainMenu";
    updateView();
})
function loadMarket() {
    loadTable("Market", "Buy", "/api/market");
}
function loadMyCards() {
    loadTable("My Card List", "Sell", "/api/myCards");
}
function loadTable(name, action, endpoint) {
    emptyCardList();
    $("#cardListName")[0].innerHTML = name;
    actionString = action;
    $("#actionString")[0].innerHTML = actionString;
    $.get(endpoint, function(data) {
        if(data) {
            if(data[0]) {
                let card = data[0].itemId;
                fillCurrentCard(card.family,card.imgUrl,card.name,card.description,card.hp,card.energy,card.defence,card.attack,(card.price/100).toFixed(2), data[0].id);
            }
            for(const card of data)
                addCardToList(card);
        }
    }).fail((res, code, msg) => {
        if(code == 401 || code == 403)
            cookieRedirect();
    });
}

function emptyCardList() {
    $("#cardListId tbody").empty();
    fillCurrentCard("","","","",0,0,0,0,0,"");
}

/**
 * List des éléments afficheables sur la page.
 * Par défault mainMenu est affiché, une view peut être forcé par l'ancre sur la page (#mainMenu)
 */
const viewList = ['#mainMenu', '#buyPage', '#sellPage']
function updateView() {
    if(!viewList.includes(window.location.hash))
        window.location.hash = "mainMenu";
    if(window.location.hash == "#mainMenu") {
        $('#sellBuyPage').toggle(false);
        $('#mainMenu').toggle(true);
    }
    else {
        $('#mainMenu').toggle(false);
        $('#sellBuyPage').toggle(true);
    }

    switch (window.location.hash) {
        case '#buyPage':
            loadMarket();
            $('#randomCardButton').toggle(true);
            break;
        case '#sellPage':
            loadMyCards();
            $('#randomCardButton').toggle(false);
            break;
    }

}
$(document ).ready(function() {
    $('.ui.dropdown').dropdown();
    updateView();
    $('#buyActiveCardButton').click(buySellCallbackCard);
    $('#randomCardButton').click(buyRandomCardCallback);
});