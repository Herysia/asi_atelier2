package com.cpe.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Atelier2 {

	public static void main(String[] args) {
		SpringApplication.run(Atelier2.class, args);
	}
}
