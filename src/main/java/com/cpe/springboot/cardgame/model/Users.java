package com.cpe.springboot.cardgame.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

/**
 * Décrit un utilisateur
 */
@Entity(name="Users")
public class Users {

    @Id
    @GeneratedValue
    private int id;

    private String name;
    @Column(unique = true)
    private String surname;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private int money;//en centimes

    public Users() {
        this.name = "";
        this.surname = "";
        this.password = "";
        this.money = 10000;
    }

    public Users(String name, String surname, String password) {
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.money = 10000;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPassword() {
        return password;
    }

    public int getMoney() {
        return money;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPassword(String password) {
        this.password = new Credentials("", password).getPassword();
    }

    public void setMoney(int money) {
        this.money = money;
    }
    public boolean removeMoney(int price) {
        money-=price;
        return money>=0;
    }

    public void addMoney(int price) {
        money+=price;
    }
}
