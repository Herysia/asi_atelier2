package com.cpe.springboot.cardgame.model;

import javax.persistence.*;


/**
 * Liste chaque carte disponible (sur le marché, possédé par un utilisateur, il peut y avoir des doublons)
 */
@Entity(name="Items")
public class Items {
    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name="itemId")
    private Cards itemId;//link with table Card

    @ManyToOne
    @JoinColumn(name="ownerId")
    private Users ownerId;//link with table User (null -> store)

    public Items() {
    }
    public Items(Cards card, Users user) {
        this.itemId = card;
        this.ownerId = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cards getItemId() {
        return itemId;
    }

    public void setItemId(Cards itemId) {
        this.itemId = itemId;
    }

    public Users getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Users ownerId) {
        this.ownerId = ownerId;
    }
}
