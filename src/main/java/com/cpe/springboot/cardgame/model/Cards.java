package com.cpe.springboot.cardgame.model;

import javax.persistence.*;

/**
 * Décrit les cartes existantes (il pourra ensuite exister plusieurs exemplaires cf Items)
 */
@Entity(name="Cards")
public class Cards {

    @Id
    @GeneratedValue
    private int id;

    private String name;
    private String description;
    private String family;
    private int hp;
    private int energy;
    private int attack;
    private int defence;
    private String imgUrl;

    private int price;

    public Cards() {
        this.name = "";
        this.description = "";
        this.family = "";
        this.hp = 0;
        this.energy = 0;
        this.attack = 0;
        this.defence = 0;
        this.imgUrl = "";
        this.price = 0;

    }

    public Cards(String name, String description, String family, int hp, int energy, int attack, int defence, String imgUrl, int price) {
        this.name = name;
        this.description = description;
        this.family = family;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.defence = defence;
        this.imgUrl = imgUrl;
        this.price = price;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getFamily() {
        return family;
    }

    public int getHp() {
        return hp;
    }

    public int getEnergy() {
        return energy;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefence() {
        return defence;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public int getPrice() {
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setDefence(int defence) {
        this.defence = defence;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
