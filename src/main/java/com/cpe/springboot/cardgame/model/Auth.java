package com.cpe.springboot.cardgame.model;


import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.UUID;

/**
 * Système d'authentification par token, un token peut être généré en fournissant les identifiants/motdepasse d'un utilisateur, et est valide 15j
 * Il est stocké sous la forme d'un cookie.
 * Un token est révoqué lorsqu'un utilisateur utilise la fonctionalité "Disconnect".
 *
 * Pour l'instant, le nombre maximal de token par utilisateur (qui correspond au nombre d'appareil) n'est pas limité, mais pourrait l'être pour éviter qu'un utilisateur sature la DB
 */
@Entity(name="Auth")
public class Auth {
    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name="userId")
    private Users userId;

    //The DB should be checked regularly for expired tokens
    private Date expiration;

    public Auth() {
        this.id = UUID.randomUUID().toString();
        //Set expiration in 15 days
        LocalDateTime local = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().plusDays(15);
        this.expiration = Date.from(local.atZone(ZoneId.systemDefault()).toInstant());
    }

    public Auth(Users user) {
        this.id = UUID.randomUUID().toString();
        //Set expiration in 15 days
        LocalDateTime local = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().plusDays(15);
        this.expiration = Date.from(local.atZone(ZoneId.systemDefault()).toInstant());
        this.userId = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getAuthToken() {
        return id;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }
}
