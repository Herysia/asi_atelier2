package com.cpe.springboot.cardgame.controller;

import com.cpe.springboot.cardgame.model.Auth;
import com.cpe.springboot.cardgame.model.Cards;
import com.cpe.springboot.cardgame.model.Credentials;
import com.cpe.springboot.cardgame.model.Users;
import org.apache.catalina.filters.ExpiresFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Auth Service: logique derrière les endpoints de AuthRestController -> Autoriser, refuser le login, créer le token etc.
 */
@Service
public class AuthService {

    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private UsersRepository usersRepository;

    public void login(Credentials credentials, HttpServletResponse response) {
        Users user = usersRepository.getByCredentials(credentials.getUsername(), credentials.getPassword());
        login(user, response);
    }
    public void login(Users user, HttpServletResponse response) {
        if(user != null) {
            login(authUser(user), response);
        }
        else
            response.setStatus(403);//Wrong/No credentials
    }
    public void login(Auth auth, HttpServletResponse response) {
        Cookie token = new Cookie("auth-token", auth.getAuthToken());
        token.setPath("/");
        token.setMaxAge(15 * 24 * 60 * 60);//15 days in seconds
        response.addCookie(token);
    }
    public void logout(String authToken, HttpServletResponse response) {
        authRepository.delete(authToken);
        deleteCookie(response);
    }
    public boolean isLoggedIn(String authToken) {
        return getAuth(authToken) != null;

    }
    public void deleteCookie(HttpServletResponse response) {
        Cookie token = new Cookie("auth-token", null);
        token.setPath("/");
        token.setMaxAge(0);
        response.addCookie(token);
    }
    public Auth authUser(Users user) {
        return authRepository.save(new Auth(user));
    }
    private Auth getAuth(String token) {
        //TODO check expiry + delete if expired
        return authRepository.findOne(token);
    }
}
