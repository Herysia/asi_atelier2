package com.cpe.springboot.cardgame.controller;

import com.cpe.springboot.cardgame.model.Auth;
import com.cpe.springboot.cardgame.model.Credentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Auth endpoints -> login, logout
 * Permet aux utilisateurs de générer des auth token qui leur donne l'accès aux endpoints/site
 * Ainsi que de les révoquer
 * (équivaut à un token de session, qui perdure dans le temps en tant qu'auth token)
 */

@RestController
public class AuthRestController {

    @Autowired
    private AuthService authService;

    @RequestMapping(method=RequestMethod.POST,value="/api/login")
    private void login(@CookieValue(value = "auth-token", defaultValue = "") String authToken, @RequestBody Credentials credentials, HttpServletResponse response) {
        //TODO: check if user is already logged in -> prevent duplicate tokens
        if(!authService.isLoggedIn(authToken))
            authService.login(credentials, response);
    }

    @RequestMapping(method=RequestMethod.GET,value="/api/logout")
    private void logout(@CookieValue(value = "auth-token", defaultValue = "") String authToken, HttpServletResponse response) {
        if(authService.isLoggedIn(authToken))
            authService.logout(authToken, response);
        else
            response.setStatus(403);//Cannot logout if not already logged in

    }

    @RequestMapping(method=RequestMethod.GET,value="/api/checkLogin")
    private void checkLogin(@CookieValue(value = "auth-token", defaultValue = "") String authToken, HttpServletResponse response) {
        if(!authService.isLoggedIn(authToken))
            authService.deleteCookie(response);
    }
}
