package com.cpe.springboot.cardgame.controller;

import com.cpe.springboot.cardgame.model.Cards;
import com.cpe.springboot.cardgame.model.Items;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * JPA interface for DB queries
 */
public interface ItemsRepository extends JpaRepository<Items, Integer> {

    @Query("select i from Items as i join i.itemId c where i.ownerId is null")
    List<Items> findByOwnerIsNull();

    @Query("select i from Items as i join i.itemId c join i.ownerId u where u.id = ?1")
    List<Items> findByUserId(int userId);
}
