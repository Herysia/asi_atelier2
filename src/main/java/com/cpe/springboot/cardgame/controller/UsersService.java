package com.cpe.springboot.cardgame.controller;

import com.cpe.springboot.cardgame.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;


    public Users getUser(int id) {
        return usersRepository.findOne(id);
    }

    public Users addUser(Users user) {
        return usersRepository.save(user);
    }

    public void updateUser(Users user) {
        usersRepository.save(user);
    }

    public void deleteUser(int id) {
        //TODO: delete auth tokens before
        usersRepository.delete(id);
    }

    public Users getUserByToken(String authToken) {
        return usersRepository.getByToken(authToken);
    }

    public Users getBySurname(String surname) {
        return usersRepository.getBySurname(surname);
    }
}
