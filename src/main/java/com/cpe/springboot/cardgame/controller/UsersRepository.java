package com.cpe.springboot.cardgame.controller;

import com.cpe.springboot.cardgame.model.Cards;
import com.cpe.springboot.cardgame.model.Credentials;
import com.cpe.springboot.cardgame.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * JPA interface for DB queries
 */
public interface UsersRepository extends JpaRepository<Users, Integer> {

    @Query("select u from Users as u where u.surname = ?1 and u.password = ?2")
    Users getByCredentials(String username, String password);

    @Query("select u from Auth as a join a.userId u  where a.id = ?1")
    Users getByToken(String token);

    @Query("select u from Users u where u.surname = ?1")
    Users getBySurname(String surname);
}
