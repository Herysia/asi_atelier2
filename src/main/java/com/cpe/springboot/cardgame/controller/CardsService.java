package com.cpe.springboot.cardgame.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.cpe.springboot.cardgame.model.Cards;
import com.cpe.springboot.cardgame.model.Items;
import com.cpe.springboot.cardgame.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardsService {

    @Autowired
    private CardsRepository cardsRepository;

    @Autowired
    private ItemsRepository itemsRepository;

    @Autowired
    private UsersRepository usersRepository;

    public List<Items> getMarketCards() {
        List<Items> items = new ArrayList<>();
        itemsRepository.findByOwnerIsNull().forEach(items::add);
        return items;
    }

    public List<Items> getMyCards(Users user) {
        List<Items> items = new ArrayList<>();
        if(user == null)
            items = null;
        else
            itemsRepository.findByUserId(user.getId()).forEach(items::add);
        return items;
    }



    public List<Cards> getAllCards() {
        List<Cards> cards = new ArrayList<>();
        cardsRepository.findAll().forEach(cards::add);
        return cards;
    }
    public Cards getCard(int id) {
        return cardsRepository.findOne(id);
    }

    public void addCard(Cards card) {
        cardsRepository.save(card);
    }

    public void updateCard(Cards card) {
        cardsRepository.save(card);

    }

    public void deleteCard(int id) {
        cardsRepository.delete(id);
    }

    public String buyCard(Users user, int itemId) {
        Items item = itemsRepository.findOne(itemId);
        if(item == null)
            return "{\"error\": \"Item does not exist\"}";
        if(item.getOwnerId()!=null)
            return "{\"error\": \"Item is not for sale\"}";
        if(!user.removeMoney(item.getItemId().getPrice()))
            return "{\"error\": \"Not enough money\"}";

        usersRepository.save(user);
        item.setOwnerId(user);
        itemsRepository.save(item);

        return "{\"success\": true}";
    }

    public String sellCard(Users user, int itemId) {
        Items item = itemsRepository.findOne(itemId);
        if(item == null)
            return "{\"error\": \"Item does not exist\"}";
        if(item.getOwnerId()!=null && item.getOwnerId().getId()!=user.getId())
            return "{\"error\": \"You don't own the item\"}";
        user.addMoney(item.getItemId().getPrice());
        usersRepository.save(user);
        item.setOwnerId(null);
        itemsRepository.save(item);

        return "{\"success\": true}";
    }

    public String buyRandomCard(Users user) {
        if(!user.removeMoney(500))//prix hardcordé todo db qqpart ?
            return "{\"error\": \"Not enough money\"}";
        int cardId = (int)(Math.random() * (((int)cardsRepository.count() - 1) + 1)) + 1;

        itemsRepository.save(new Items(cardsRepository.findOne(cardId), user));


        return "{\"success\": true}";
    }



    /*TODO:
        getCardByName
        getCardById
        getCardByKeyword
    */
}
