package com.cpe.springboot.cardgame.controller;

import com.cpe.springboot.cardgame.model.Auth;
import com.cpe.springboot.cardgame.model.Credentials;
import com.cpe.springboot.cardgame.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * JPA interface for DB queries
 */
public interface AuthRepository extends JpaRepository<Auth, String> {

}
