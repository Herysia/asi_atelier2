package com.cpe.springboot.cardgame.controller;

import com.cpe.springboot.cardgame.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
public class UsersRestController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private AuthService authService;

    @RequestMapping(method=RequestMethod.GET,value="/api/getUserData")
    private Users getUserData(@CookieValue(value = "auth-token", defaultValue = "") String authToken, HttpServletResponse response) {
        if(!authService.isLoggedIn(authToken)) {
            authService.deleteCookie(response);
            return null;
        }
        else {
            return usersService.getUserByToken(authToken);
        }
    }

    @RequestMapping(method=RequestMethod.POST,value="/api/register")
    private void register(@CookieValue(value = "auth-token", defaultValue = "") String authToken, @RequestBody Users user,  HttpServletResponse response) {
        //TODO: server side data validation (long enough password, etc)
        if( usersService.getBySurname(user.getSurname()) == null) {
            usersService.addUser(user);
            authService.login(user, response);
            response.setStatus(201);
        }
        else
            response.setStatus(409);
    }
}
