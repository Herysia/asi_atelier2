package com.cpe.springboot.cardgame.controller;

import java.util.List;

import com.cpe.springboot.cardgame.model.Cards;
import com.cpe.springboot.cardgame.model.Items;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;


@RestController
public class CardsRestController {

    @Autowired
    private CardsService cardsService;

    @Autowired
    private AuthService authService;

    @Autowired
    private UsersService usersService;

    @RequestMapping("/api/market")
    private List<Items> getMarketCards(@CookieValue(value = "auth-token", defaultValue = "") String authToken, HttpServletResponse response) {
        if(!authService.isLoggedIn(authToken)) {
            authService.deleteCookie(response);
            response.setStatus(401);
            return null;
        }
        else
            return cardsService.getMarketCards();
    }
    @RequestMapping("/api/myCards")
    private List<Items> getMyCards(@CookieValue(value = "auth-token", defaultValue = "") String authToken, HttpServletResponse response) {
        if(!authService.isLoggedIn(authToken)) {
            authService.deleteCookie(response);
            response.setStatus(401);
            return null;
        }
        else
            return cardsService.getMyCards(usersService.getUserByToken(authToken));
    }
    @RequestMapping("/api/buyCard")
    private String buyCard(@CookieValue(value = "auth-token", defaultValue = "") String authToken, @RequestParam int id,  HttpServletResponse response) {
        if(!authService.isLoggedIn(authToken)) {
            authService.deleteCookie(response);
            response.setStatus(401);
            return null;
        }
        else
            return cardsService.buyCard(usersService.getUserByToken(authToken), id);
    }
    @RequestMapping("/api/buyRandomCard")
    private String buyCard(@CookieValue(value = "auth-token", defaultValue = "") String authToken, HttpServletResponse response) {
        if(!authService.isLoggedIn(authToken)) {
            authService.deleteCookie(response);
            response.setStatus(401);
            return null;
        }
        else
            return cardsService.buyRandomCard(usersService.getUserByToken(authToken));
    }

    @RequestMapping("/api/sellCard")
    private String sellCard(@CookieValue(value = "auth-token", defaultValue = "") String authToken, @RequestParam int id,  HttpServletResponse response) {
        if(!authService.isLoggedIn(authToken)) {
            authService.deleteCookie(response);
            response.setStatus(401);
            return null;
        }
        else
            return cardsService.sellCard(usersService.getUserByToken(authToken), id);
    }
    /**
     * Never Used delete ?
     */
    /*
    @RequestMapping("/cards/{id}")
    private Cards getCard(@PathVariable String id) {
        return cardsService.getCard(id);

    }
    */
    /**
     * We could create an admin API that allow creating card
     */
    /*
    @RequestMapping(method=RequestMethod.POST,value="/api/cards")
    public void addCard(@RequestBody Cards card) {
        cardsService.addCard(card);
    }
    */
    /**
     * We could create an admin API that allow editing existing cards
     */
    /*
    @RequestMapping(method=RequestMethod.PUT,value="/api/cards/{id}")
    public void updateCard(@RequestBody Cards card,@PathVariable String id) {
        card.setId(id);
        cardsService.updateCard(card);
    }
    */
    /**
     * We could create an admin API that allow removing card
     */
    /*
    @RequestMapping(method=RequestMethod.DELETE,value="/api/cards/{id}")
    public void deleteCard(@PathVariable String id) {
        cardsService.deleteCard(id);
    }
     */
}
