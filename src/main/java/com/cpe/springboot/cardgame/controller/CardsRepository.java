package com.cpe.springboot.cardgame.controller;

import java.util.List;

import com.cpe.springboot.cardgame.model.Cards;
import com.cpe.springboot.cardgame.model.Items;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * JPA interface for DB queries
 */
public interface CardsRepository extends JpaRepository<Cards, Integer> {


}
